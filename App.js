import React from 'react';
import {SafeAreaView, View, StyleSheet} from 'react-native';

import Calendar from './src/components/Calendar';

const App2 = () => {
  return (
    <SafeAreaView style={styles.container0}>
      <View style={styles.container}>
        <Calendar />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container0: {
    backgroundColor: '#000000',
    height: '100%',
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#000000',
  },
});

export default App2;
