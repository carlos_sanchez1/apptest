import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, FlatList, Dimensions} from 'react-native';
import {MediaQueryStyleSheet} from 'react-native-responsive';

import I18n from '../../services/translation';

import Task from '../Task';

const Calendar = () => {
  //Año y mes modificables/cambiantes con la interacción de las flechas del calendario 'prev, next'
  const [
    modifiableCurrentYearNumber,
    setModifiableCurrentYearNumber,
  ] = useState(0);
  const [
    modifiableCurrentMonthNumber,
    setModifiableCurrentMonthNumber,
  ] = useState(0);

  const [currentYear, setCurrentYear] = useState(0);
  const [currentMonth, setCurrentMonth] = useState(0);
  const [currentDay, setCurrentDay] = useState(0);
  const [monthName, setMonthName] = useState('');
  const [days, setDays] = useState([]);

  useEffect(() => {
    const date = new Date();
    setModifiableCurrentYearNumber(date.getFullYear());
    setCurrentYear(date.getFullYear());
    setModifiableCurrentMonthNumber(date.getMonth());
    setCurrentMonth(date.getMonth());
    setCurrentDay(date.getDate());
    console.log(Dimensions.get('window').width);
  }, []);

  useEffect(() => {
    switch (modifiableCurrentMonthNumber) {
      case 0:
        setMonthName(I18n.t('month0'));
        break;
      case 1:
        setMonthName(I18n.t('month1'));
        break;
      case 2:
        setMonthName(I18n.t('month2'));
        break;
      case 3:
        setMonthName(I18n.t('month3'));
        break;
      case 4:
        setMonthName(I18n.t('month4'));
        break;
      case 5:
        setMonthName(I18n.t('month5'));
        break;
      case 6:
        setMonthName(I18n.t('month6'));
        break;
      case 7:
        setMonthName(I18n.t('month7'));
        break;
      case 8:
        setMonthName(I18n.t('month8'));
        break;
      case 9:
        setMonthName(I18n.t('month9'));
        break;
      case 10:
        setMonthName(I18n.t('month10'));
        break;
      case 11:
        setMonthName(I18n.t('month11'));
        break;
      default:
        setMonthName('0000');
        break;
    }

    const handleLeapYear = () => {
      // eslint-disable-next-line prettier/prettier
      return ((modifiableCurrentYearNumber % 100 !== 0) && (modifiableCurrentYearNumber % 4 === 0) || (modifiableCurrentYearNumber % 400 === 0));
    };

    const handleMonthDays = () => {
      if (
        modifiableCurrentMonthNumber === 0 ||
        modifiableCurrentMonthNumber === 2 ||
        modifiableCurrentMonthNumber === 4 ||
        modifiableCurrentMonthNumber === 6 ||
        modifiableCurrentMonthNumber === 7 ||
        modifiableCurrentMonthNumber === 9 ||
        modifiableCurrentMonthNumber === 11
      ) {
        return 31;
      } else if (
        modifiableCurrentMonthNumber === 3 ||
        modifiableCurrentMonthNumber === 5 ||
        modifiableCurrentMonthNumber === 8 ||
        modifiableCurrentMonthNumber === 10
      ) {
        return 30;
      } else {
        return handleLeapYear() ? 29 : 28;
      }
    };

    class dayobj {
      constructor(day, month, year, id, otherdaysmonth) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.id = id;
        this.otherdaysmonth = otherdaysmonth;
      }
    }

    const handleWriteMonth = () => {
      const prevLastDay = new Date(
        modifiableCurrentYearNumber,
        modifiableCurrentMonthNumber,
        0,
      ).getDate();

      const handleStartDayWeek = new Date(
        modifiableCurrentYearNumber,
        modifiableCurrentMonthNumber,
        1,
      ).getDay();

      const lastDayIndex = new Date(
        modifiableCurrentYearNumber,
        modifiableCurrentMonthNumber + 1,
        0,
      ).getDay();

      const nextDays = 7 - lastDayIndex - 1;

      const totaldays = [];
      for (let i = handleStartDayWeek; i > 0; i--) {
        totaldays.push(
          new dayobj(
            prevLastDay - i + 1,
            modifiableCurrentMonthNumber - 1,
            modifiableCurrentYearNumber,
            prevLastDay - i + 1,
            true,
          ),
        );
      }

      for (let i = 1; i <= handleMonthDays(); i++) {
        totaldays.push(
          new dayobj(
            i,
            modifiableCurrentMonthNumber,
            modifiableCurrentYearNumber,
            i,
            false,
          ),
        );
      }

      for (let i = 1; i <= nextDays; i++) {
        totaldays.push(
          new dayobj(
            i,
            modifiableCurrentMonthNumber + 1,
            modifiableCurrentYearNumber,
            i,
            true,
          ),
        );
      }
      setDays(totaldays);
    };
    handleWriteMonth();
  }, [modifiableCurrentMonthNumber, modifiableCurrentYearNumber]);

  const handleLastMonth = () => {
    if (modifiableCurrentMonthNumber !== 0) {
      setModifiableCurrentMonthNumber(modifiableCurrentMonthNumber - 1);
    } else {
      setModifiableCurrentMonthNumber(11);
      setModifiableCurrentYearNumber(modifiableCurrentYearNumber - 1);
    }
  };

  const handleNextMonth = () => {
    if (modifiableCurrentMonthNumber !== 11) {
      setModifiableCurrentMonthNumber(modifiableCurrentMonthNumber + 1);
    } else {
      setModifiableCurrentMonthNumber(0);
      setModifiableCurrentYearNumber(modifiableCurrentYearNumber + 1);
    }
  };

  const daysWeek = [
    {day: I18n.t('1'), id: 1},
    {day: I18n.t('2'), id: 2},
    {day: I18n.t('3'), id: 3},
    {day: I18n.t('4'), id: 4},
    {day: I18n.t('5'), id: 5},
    {day: I18n.t('6'), id: 6},
    {day: I18n.t('7'), id: 7},
  ];

  return (
    <View style={styles.container}>
      <View style={styles.calendar}>
        <View style={styles.calendar_info}>
          <TouchableOpacity onPress={handleLastMonth}>
            <Text style={styles.calendar_info_icon}>&#9664;</Text>
          </TouchableOpacity>
          <View style={styles.calendar_month_year}>
            <Text style={styles.calendar_info_txt}>{monthName}</Text>
            <Text style={styles.calendar_info_txt}>
              {modifiableCurrentYearNumber}
            </Text>
          </View>
          <TouchableOpacity onPress={handleNextMonth}>
            <Text style={styles.calendar_info_icon}>&#9654;</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.calendar_week}>
          <FlatList
            data={daysWeek}
            keyExtractor={(item) => item.id}
            numColumns={7}
            scrollEnabled={false}
            renderItem={({item}) => (
              <Text style={styles.dayWeek}>{item.day}</Text>
            )}
          />
        </View>

        <View style={styles.calendar_week}>
          <FlatList
            data={days}
            keyExtractor={(item) => item.id}
            style={styles.calendar_week_days}
            numColumns={7}
            renderItem={({item}) =>
              item.day === currentDay &&
              item.month === currentMonth &&
              item.year === currentYear ? (
                <TouchableOpacity>
                  <Text style={styles.currentDayWeek}>{item.day}</Text>
                </TouchableOpacity>
              ) : item.otherdaysmonth ? (
                <TouchableOpacity>
                  <Text style={styles.pastDaysWeek}>{item.day}</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity>
                  <Text style={styles.daysWeek}>{item.day}</Text>
                </TouchableOpacity>
              )
            }
          />
        </View>

        <View style={styles.taskContainer}>
          <Task />
        </View>
      </View>
    </View>
  );
};

const windowWidth = Dimensions.get('window').width - 26;
const itemWidth = windowWidth / 7;
const styles = MediaQueryStyleSheet.create(
  {
    container: {
      alignItems: 'center',
      width: '94%',
    },
    calendar: {
      width: '100%',
    },
    calendar_info: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginTop: '3%',
      marginBottom: '6%',
    },
    calendar_info_icon: {
      fontSize: 25,
      color: '#FFFFFF',
    },
    calendar_info_txt: {
      fontSize: 22,
      letterSpacing: -1,
      color: '#FFFFFF',
    },
    calendar_month_year: {
      width: '28%',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    calendar_week: {
      flexDirection: 'row',
    },
    dayWeek: {
      fontSize: 15,
      textAlign: 'center',
      width: itemWidth,
      marginBottom: '4%',
      color: '#FFFFFF',
    },
    calendar_week_days: {
      width: '100%',
    },
    daysWeek: {
      textAlign: 'center',
      width: itemWidth,
      paddingVertical: 12,
      fontSize: 17,
      color: '#FFFFFF',
      borderRadius: 28,
      overflow: 'hidden',
      // backgroundColor: '#121212',
    },
    pastDaysWeek: {
      textAlign: 'center',
      width: itemWidth,
      paddingVertical: 12,
      fontSize: 17,
      color: '#383838',
      borderRadius: 28,
      overflow: 'hidden',
      // backgroundColor: '#121212',
    },
    currentDayWeek: {
      textAlign: 'center',
      width: itemWidth,
      paddingVertical: 12,
      backgroundColor: '#6925F8',
      color: 'white',
      borderRadius: 28,
      overflow: 'hidden',
      fontSize: 17,
    },
    taskContainer: {
      // marginTop: '15%',
    },
  },
  {
    '@media (min-device-width: 414) and (max-device-height: 2961)': {
      currentDayWeek: {
        paddingVertical: 15.5,
        marginBottom: 0.5,
      },
      daysWeek: {
        paddingVertical: 15.5,
        marginBottom: 0.5,
      },
      pastDaysWeek: {
        paddingVertical: 15.5,
        marginBottom: 0.5,
      },
    },
  },
);

export default Calendar;
