import React, {useState, useRef} from 'react';
import {View, Text, TouchableOpacity, TextInput} from 'react-native';

import getRealm from '../../services/realm';
import I18n from '../../services/translation';
//FALTA LO DE LOS STYLES!!!!!!!!
//FUNCION MODAL AL ABRIR TODOS LOS CAMPOS VACIOS
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

import RBSheet from 'react-native-raw-bottom-sheet';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import SwitchSelector from 'react-native-switch-selector';

const CreateTaskModal = () => {
  const [inputNameTask, setInputNameTask] = useState('');
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const refRBSheet = useRef();

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.warn(
      'A date has been picked: ',
      date.getHours(),
      date.getMinutes(),
    );
    hideDatePicker();
  };

  const handleCreateTask = async () => {
    const data = {
      id: parseInt(uuidv4()),
      name: inputNameTask,
    };
    const realm = await getRealm();

    realm.write(() => {
      realm.create('Task', data);
    });
  };

  const handleSaveNewTask = async () => {
    try {
      await handleCreateTask();

      setInputNameTask('');
    } catch (error) {
      console.log('ERR', error);
    }
  };

  const options = [
    {label: 'Notification', value: '4'},
    {label: 'Alarm', value: '5'},
  ];

  const options2 = [
    {label: 'Low', value: '1', activeColor: '#2ED27C'},
    {label: 'Half', value: '2', activeColor: '#FFD918'},
    {label: 'High', value: '3', activeColor: '#FE354B'},
  ];

  return (
    <View style={styles.conatiner}>
      <Text style={styles.questionTxt}>{I18n.t('question')}</Text>
      <Text style={styles.addTxt}>{I18n.t('add')}</Text>
      <TouchableOpacity
        onPress={() => refRBSheet.current.open()}
        style={styles.btn}>
        {/* <Icon name="plus" size={49} /> */}
        <Text>+</Text>
      </TouchableOpacity>
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={550}
        animationType="fade"
        customStyles={{
          wrapper: {
            backgroundColor: 'rgba(18, 18, 18, 0.8)',
          },
          container: {
            borderTopLeftRadius: 40,
            borderTopRightRadius: 40,
            backgroundColor: '#1E1E1E',
          },
        }}>
        <View>
          <Text style={styles.modalTxt}>{I18n.t('new')}</Text>
          <View style={styles.modalContentView}>
            <Text style={styles.nameModalTxt}>{I18n.t('name')}</Text>
            <TextInput
              style={styles.modalInput}
              placeholderTextColor="#ADADAF"
              placeholder={I18n.t('title')}
              value={inputNameTask}
              onChangeText={(text) => setInputNameTask(text)}
            />
            <Text style={styles.modalBtnColorsTxt}>{I18n.t('colorI')}</Text>
            <SwitchSelector
              options={options2}
              initial={0}
              hasPadding
              borderColor="white"
              selectedColor="white"
              buttonMargin={1}
              backgroundColor="gray"
              fontSize={12}
              height={40}
              onPress={(value) =>
                console.log(`Call onPress with value: ${value}`)
              }
            />
            <View style={styles.timeTxtAndSwitchContainer}>
              <Text style={styles.timeTxt}>{I18n.t('time')}</Text>
              <View style={styles.switchContainer}>
                <SwitchSelector
                  options={options}
                  initial={0}
                  hasPadding
                  borderColor="white"
                  buttonColor="white"
                  selectedColor="black"
                  buttonMargin={1}
                  backgroundColor="gray"
                  fontSize={9}
                  height={30}
                  onPress={(value) =>
                    console.log(`Call onPress with value: ${value}`)
                  }
                />
              </View>
            </View>
            <TouchableOpacity onPress={showDatePicker} style={styles.timeBtn}>
              <Text style={styles.timeBtnTxt}>7:50 AM</Text>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              mode="time"
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
            />
            <Text style={styles.iconsTxt}>{I18n.t('icons')}</Text>
            <TouchableOpacity>
              <Text style={styles.createTxt}>{I18n.t('create')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

export default CreateTaskModal;
