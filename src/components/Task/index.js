import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  FlatList,
  TouchableHighlight,
} from 'react-native';
import {MediaQueryStyleSheet} from 'react-native-responsive';
import I18n from '../../services/translation';
//LIMPIAR Y AUTOMATIZAR TODO!!!!!!!!
import Swipeable from 'react-native-swipeable';

const Task = () => {
  const [todoList, setTodoList] = useState([]);
  const [name, setName] = useState('');

  const tasks = [
    {name: 'task 1', color: '#2ED27C', time: '11 AM', id: 1},
    {name: 'task 2', color: '#FFD918', time: '12 PM', id: 2},
    {name: 'task 3', color: '#FE354B', time: '13 PM', id: 3},
    {name: 'task 4', color: '#2ED27C', time: '14 PM', id: 4},
    {name: 'task 5', color: '#FFD918', time: '15 PM', id: 5},
  ];

  console.log('name:', name);

  const handleCreateTaskView = () => {};

  useEffect(() => {
    const handleShowTasks = async () => {
      const realm = await getRealm();

      const data = realm.objects('Task');

      setTodoList(data);
    };
    handleShowTasks();
  }, []);

  const handleShowTasksView = () => {
    return (
      <View style={styles.conatiner}>
        <View style={styles.conatiner15}>
          <View style={styles.conatiner2}>
            <Text style={styles.dayWeek}>Today</Text>
            <View style={styles.conatiner3}>
              <TouchableOpacity style={styles.btn2}>
                <Text>...</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => refRBSheet.current.open()}
                style={styles.btn2}>
                <Text>+</Text>
              </TouchableOpacity>
              
            </View>
          </View>
          <View>
            <FlatList
              data={todoList}
              keyExtractor={(item) => item.id}
              numColumns={1}
              scrollEnabled={true}
              renderItem={({item}) => (
                <Swipeable
                  leftActionActivationDistance={100}
                  leftContent={
                    <TouchableHighlight
                      // eslint-disable-next-line react-native/no-inline-styles
                      style={{
                        backgroundColor: item.color,
                        paddingVertical: 31,
                        paddingHorizontal: 50,
                        borderRadius: 190,
                        marginTop: 20,
                        marginRight: 15,
                      }}>
                      <Text>Button 1</Text>
                    </TouchableHighlight>
                  }
                  rightButtonWidth={100}
                  rightButtons={[
                    <TouchableHighlight
                      // eslint-disable-next-line react-native/no-inline-styles
                      style={{
                        backgroundColor: item.color,
                        paddingVertical: 31,
                        paddingHorizontal: 50,
                        borderRadius: 190,
                        marginTop: 20,
                        marginLeft: 15,
                      }}>
                      <Text>Button 2</Text>
                    </TouchableHighlight>,
                  ]}>
                  <TouchableOpacity>
                    <View
                      // eslint-disable-next-line react-native/no-inline-styles
                      style={{
                        backgroundColor: item.color,
                        paddingVertical: 21,
                        paddingHorizontal: 50,
                        borderRadius: 190,
                        marginTop: 20,
                      }}>
                      <Text style={styles.dayWeek}>{item.name}</Text>
                      {/* <Text style={styles.dayWeek}>{item.time}</Text> */}
                    </View>
                  </TouchableOpacity>
                </Swipeable>
              )}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView>
      <View>
        {todoList <= 0 ? handleCreateTaskView() : handleShowTasksView()}
      </View>
    </SafeAreaView>
  );
};

const styles = MediaQueryStyleSheet.create(
  {
    conatiner: {
      marginTop: '10%',
      // alignItems: 'center',
      // backgroundColor: 'blue',
      alignItems: 'center',
    },
    conatiner15: {
      width: '91%',
    },
    conatiner2: {
      flexDirection: 'row',
      // backgroundColor: 'red',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    conatiner3: {
      width: '15%',
      flexDirection: 'row',
      justifyContent: 'space-between',
      // backgroundColor: 'green',
    },
    btn2: {
      backgroundColor: '#3EDAF0',
      padding: 5,
      borderRadius: 100,
    },
    dayWeek: {
      color: 'white',
    },
    questionTxt: {
      fontSize: 25,
      color: '#FFFFFF',
    },
    addTxt: {
      textAlign: 'center',
      fontSize: 17,
      marginTop: 13,
      color: '#FFFFFF',
      marginBottom: 13,
    },
    btn: {
      backgroundColor: '#59EEFF',
      borderRadius: 150,
      paddingHorizontal: 7,
      paddingVertical: 6,
    },
    modalContainer: {
      backgroundColor: 'rgba(18, 18, 18, 0.8)',
      height: '100%',
      flexDirection: 'column-reverse',
    },
    modalContentView: {
      paddingTop: 25,
      paddingLeft: 35,
      paddingRight: 35,
    },
    modalTxt: {
      fontSize: 18,
      alignSelf: 'center',
      color: '#FFFFFF',
    },
    nameModalTxt: {
      fontSize: 16,
      marginBottom: 5,
      color: '#ffffff',
    },
    modalInput: {
      paddingVertical: 9,
      paddingHorizontal: 13,
      borderWidth: 1,
      borderColor: '#EDEBEA',
      backgroundColor: 'gray',
      borderRadius: 8,
      marginBottom: 20,
      color: 'white',
    },
    modalBtnColorsTxt: {
      fontSize: 16,
      marginBottom: 20,
      color: '#ffffff',
    },
    timeTxt: {
      fontSize: 16,
      color: '#ffffff',
    },
    timeBtn: {
      marginTop: 15,
      backgroundColor: 'grey',
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 18,
      paddingRight: 18,
      borderRadius: 8,
      shadowColor: '#fff',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5,
      marginBottom: 20,
    },
    timeTxtAndSwitchContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 25,
    },
    timeBtnTxt: {
      color: 'white',
    },
    switchContainer: {
      width: 135,
      marginLeft: 15,
    },
    iconsTxt: {
      fontSize: 16,
      color: '#ADADAF',
    },
    createTxt: {
      fontSize: 16,
      color: '#1FF2FF',
    },
  },
  {
    '@media (min-device-width: 414) and (max-device-height: 2961)': {
      btn: {
        borderRadius: 100,
        paddingHorizontal: 9,
        paddingVertical: 8,
      },
    },
  },
);

export default Task;
