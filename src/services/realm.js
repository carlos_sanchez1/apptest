import Realm from 'realm';

import TaskSchema from '../schemas/TaskSchema';

export default function getRealm() {
  return Realm.open({
    schema: [TaskSchema],
  });
}
