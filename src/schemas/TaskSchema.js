export default class TaskSchema {
  static schema = {
    name: 'Task',
    primaryKey: 'id',
    properties: {
      id: {type: 'int', indexed: true},
      name: 'string',
    },
  };
}
